#!/bin/bash
cd /root/builds/qzuEByCL/0/mohrezaeib/awin-backend
python3 -m venv env
source /root/builds/qzuEByCL/0/mohrezaeib/awin-backend/env/bin/activate
pip install -r requirements.txt
#python3 manage.py collectstatic --noinput
nohup python3 manage.py runserver &